import ProfileHistoryItem from "./ProfileHistoryItem"

const ProfileHistory = ({translations}) => {

    const translationList = translations.map(
        (translation) => <ProfileHistoryItem key={translation} translation={translation} />)
    
    return (
        <section>
         <h4>Your translation history</h4>
         <ul>
          {translationList}  
         </ul>    
            
        </section>
    )
}
export default ProfileHistory