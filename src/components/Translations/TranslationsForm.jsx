import { useForm } from "react-hook-form";


const TranslationsForm = ({ onTranslate }) => {
    const { register, handleSubmit } = useForm();

    const onSubmit = ({ translationInput }) => {
        onTranslate(translationInput);
    };

    return (
        <form onSubmit={handleSubmit(onSubmit)} className="customForm">
            <fieldset>
                <label htmlFor="username"> </label>
                
                    
                    <input
                        id="inlineFormInputGroup"
                        placeholder="Insert text to translate"
                        {...register("translationInput")}
                    >
                        </input>
                   
                        <button variant="primary" type="submit">
                            translate
                        </button>
        
                
            </fieldset>
        </form>
    );
};

export default TranslationsForm;
