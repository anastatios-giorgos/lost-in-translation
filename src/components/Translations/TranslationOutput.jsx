const TranslationOutput = ({ sign }) => {
    return <img src={sign.image} alt="Sign icon" />;
};
 
export default TranslationOutput

