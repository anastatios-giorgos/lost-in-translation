# Lost in Translation

Αn online sign language translator as a Single Page Application using the React framework.

## Table of Contents

- [About](#about)
- [Install](#install)
- [Usage](#usage)
- [Built with](#built-with)
- [Contributing](#contributing)

## About

#### Startup page

The first thing a user should see is the “Login page” where the user must be able to enter their name.
Save the username to the Translation API. Once the name has been stored in the API, the app must display the main page, the translation page. Users that are already logged in may automatically be redirected to the Translation page. You may use the browsers’ local storage to manage the session.

#### Translation page

A user may only view this page if they are currently logged into the app. Please redirect a user back to the login page if no active login session exists in the browser storage.
The user types in the input box of the page. The user must click on the “translate” button to trigger the translation.Translations must be stored using the API . The Sign language characters
must appear in the “translated” box.

#### Profile page

The profile page must display the last 10 translations for the current user.
There must also be a button to clear the translations. This should “delete” in your API and no longer display on the profile page.
The Logout button should clear all the storage and return to the start page.

## Install

To run the application locally: clone repository build project.

## Usage

Run locally with npm start.

## Built with

• Figma
• NPM/Node.js (LTS – Long Term Support version)
• React CRA (create-react-app)
• Visual Studio Code Text Editor
• Browser Developer Tools for testing and debugging
• React
• Git
• Rest API

## Contributing

- [Giorgos Tzafilkos](https://gitlab.com/tzaf.giorgos)
- [Kamperopoulos Anastasios](https://gitlab.com/KamperopoulosA)

PRs accepted.
